(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

let debug = 0

(****************************************************************)
(* Simple types                                                 *)
(****************************************************************)

(* *Deep* embedding of types:

<<<
    τ, σ ::= Ω
           | τ → σ
>>>
 *)

type tp =
  | Base
  | Arr of tp * tp
[@@deriving show]

(* Define [typ1] as [Ω → Ω] *)

let typ1 () = Arr (Base, Base)

(* Define [typ2] as [(Ω → Ω) → Ω → Ω] *)

let typ2 () = Arr (typ1(), typ1())

let _ =
  if debug > 0
  then (Format.printf "tp1 = %a\n" pp_tp (typ1 ()) ;
        Format.printf "tp2 = %a\n" pp_tp (typ2 ()) )


(****************************************************************)
(* Source language: λ-terms                                     *)
(****************************************************************)

(* *Deep* embedding of terms using weak/parametric higher-order
   abstract syntax (HOAS):

<<<
    t, u ::= x
           | λ x. t
           | t u
>>>
 *)

type 'a tm = Var of 'a
           | Lam of 'a -> 'a tm
           | App of 'a * 'a

(* NYI *)
(* Hint: ['a] ranges over the set of variables,
         piggy-back on OCaml for managing binders! *)

let rec pp_tm gensym pp_var oc tm =
  failwith "NYI"
  (* (match tm with
   * | Var x -> ?
   * | Lam f -> ?
   * | App (s,t) -> ?) *)

(* Define [tm1] as [λ x. x] *)

let tm1 () = Lam (fun x -> Var x)

(* Define [tm2] as [λ f. λ x. f x] *)

let tm2 () = Lam (fun f -> Lam (fun x -> App (f,x)))

(* Define [tm3] as [λ x. (λ y. y) x] *)
let tm3 () = Lam (fun x -> App (Lam (fun y -> y), x))

let _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      "x" ^ string_of_int !x
  in
  let pp_var oc s = Format.fprintf oc "%s" s in
  let pp_tm_str = pp_tm gensym pp_var in
  if debug > 1
  then (
    Format.printf "tm1 = %a\n" pp_tm_str (tm1 ()) ;
    Format.printf "tm2 = %a\n" pp_tm_str (tm2 ()) ;
    Format.printf "tm3 = %a\n" pp_tm_str (tm3 ()) )


(****************************************************************)
(* Intermediate language of values                              *)
(****************************************************************)

(* *Shallow* embedding of values in weak-head normal form:
   [https://en.wikipedia.org/wiki/Lambda_calculus_definition#Weak_head_normal_form]

<<<
    v ::= λ x. v
        | base

>>>
 *)

(* We are polymorphic in the representation ['b] of base values for now *)

type 'b vl =
  | VFun of ('b vl -> 'b vl)
  | VBase of 'b

(* Define [vl1] as [λ x. x] *)

let vl1 () = failwith "NYI"

(* Define [vl2] as [λ f. λ x. f x] *)

let vl2 () = failwith "NYI"

(* Define [vl3] as [λ x. (λ y. y) x] *)
let vl3 () = failwith "NYI"

(****************************************************************)
(* Target language: β-normal λ-terms                            *)
(****************************************************************)

(* *Deep* embedding of β-normal terms using weak/parametric
   higher-order abstract syntax (HOAS):
   [https://en.wikipedia.org/wiki/Beta_normal_form]

<<<
    nf ::= at
         | λ x. nf

    at ::= at nf
         | x
>>>
*)

(* Hint: ['a] ranges over the set of variables,
   piggy-back on OCaml for managing binders! *)

type 'a nf = |

(* NYI *)
and 'a at = |

(* NYI *)

let rec pp_nf gensym pp_var oc nf = failwith "NYI"

and pp_at gensym pp_var oc at = failwith "NYI"

let rec equal_nf gensym equal_var nf1 nf2 = failwith "NYI"

and equal_at gensym equal_var at1 at2 = failwith "NYI"

(* Define [nf1] as [λ x. x] *)

let nf1 () = failwith "NYI"

(* Define [nf2] as [λ f. λ x. f x] *)

let nf2 () = failwith "NYI"

(* Define [nf3] as [λ x. (λ y. y) x] *)
let nf3 () = failwith "NYI"

let _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      "x" ^ string_of_int !x
  in
  let pp_var oc s = Format.fprintf oc "%s" s in
  let pp_nf_str = pp_nf gensym pp_var in
  if debug > 2
  then (
    Format.printf "nf1 = %a\n" pp_nf_str (nf1 ()) ;
    Format.printf "nf2 = %a\n" pp_nf_str (nf2 ()) ;
    Format.printf "nf3 = %a\n" pp_nf_str (nf3 ()) )


let%test _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      !x
  in
  let nf1 = nf1 () in
  let nf2 = nf2 () in
  not (equal_nf gensym ( = ) nf1 nf2)

(****************************************************************)
(* Evaluation function: from source to intermediate             *)
(****************************************************************)

let rec eval : type a. a vl tm -> a vl = fun _ -> failwith "NYI"

(****************************************************************)
(* reify and reflect: from intermediate to target               *)
(****************************************************************)

let rec reify : type a. tp -> a at vl -> a nf = fun a v -> failwith "NYI"

and reflect : type a. tp -> a at -> a at vl = fun a r -> failwith "NYI"

let%test _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      !x
  in
  let n1 = reify (Arr (Base, Base)) (VFun (fun x -> x)) in
  let n2 = nf1 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      !x
  in
  let n1 =
    reify (Arr (Arr (Base, Base), Arr (Base, Base))) (VFun (fun x -> x))
  in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

(****************************************************************)
(* Normalization: from term to normal form                      *)
(****************************************************************)

let nbe : type a. tp -> a at vl tm -> a nf = fun a m -> failwith "NYI"

let gensym =
  let x = ref 0 in
  fun () ->
    incr x ;
    !x


let%test _ =
  let typ1 = typ1 () in
  let tm1 = tm1 () in
  let n1 = nbe typ1 tm1 in
  let n2 = nf1 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ2 = typ2 () in
  let tm2 = tm2 () in
  let n1 = nbe typ2 tm2 in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ2 = typ2 () in
  let tm1 = tm1 () in
  let n1 = nbe typ2 tm1 in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ1 = typ1 () in
  let tm3 = tm3 () in
  let n1 = nbe typ1 tm3 in
  let n2 = nf1 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ2 = typ2 () in
  let tm3 = tm3 () in
  let n1 = nbe typ2 tm3 in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

(****************************************************************)
(* Public API                                                   *)
(****************************************************************)

type vars

type x = vars at vl

type term = x tm

type normal = vars nf

let norm : tp -> term -> normal = nbe
